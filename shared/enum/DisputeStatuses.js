module.exports = {
  incomplete: 'Incomplete',
  completed: 'Completed',
  inReview: 'In Review',
  documentsSent: 'Documents Sent',
  update: 'Update',
  userUpdate: 'User Update',
};
